package com.gitlab.fisvse.adventura_sem_pesd02.application;

import com.gitlab.fisvse.adventura_sem_pesd02.logika.*;

import com.gitlab.fisvse.adventura_sem_pesd02.uiText.TextoveRozhrani;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			//vytvoření nové instance loaderu namísto využití statické metody (minulý přístup)
			FXMLLoader loader = new FXMLLoader();
			
			//načtení souboru s FXML pomocí getResource - vyřeší cesty v rámci balíčku, používá se hlavně u Maven projektů
			//u cesty k FXML na adresář main/resources bylo nutné napsat před název souboru lomítko, protože se nachází v kořeni místa, kde Maven očekává zdroje, předtím, když to byla relativní cesta (v adresáři s kódem) to nebylo nutné
			loader.setLocation(getClass().getResource("Sample.fxml"));
			
			//načtení kořenového prvku grafiky (FXML), lze přetypovat na konkrétní typ (GridPane), ale v tomto případě to nebylo nutné, protože nepoužíváme žádné specifické metody GridPane
			Parent root = loader.load();
			
			//získání odkazu na náš controller od loaderu
			SampleController c = loader.getController();			
			
			//vytvoření instance nové scény z velikostí okna
			Scene scene = new Scene(root, 800, 600);
			
			//načtení kaskádových stylů z adresáře main/resources
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			//přiřazení scény na stage, všimňete si, že stage jsme nevytvářeli, dostali jsme jí jako parametr
			primaryStage.setScene(scene);
			
			//zobrazení stage a tím celého okna
			primaryStage.show();
			
			//nová instance hry, nemá cenu používat stejnou jako pro textový režim (v metodě main)
			IHra hra = new Hra();
			
			//předání instance hry do kontroleru, metoda inicializuj je naše vlastní, tím, že jí voláme zde v metodě start, nehrozí, že dostaneme null pointer během používání grafiky
			c.inicializuj(hra);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		//kontrola počtu parametrů
		if (args.length == 0) {
			//spuštění grafiky, parametr args je povinný, ale v tomto případě už ho v grafice nepoužijeme
			launch(args);
		} else {
			//konrola prvního parametru a jeho obsahu
			if (args[0].equals("-text")) {
				//inicializace nové hry
				IHra hra = new Hra();
				//inicializace textového rozhraní
				TextoveRozhrani ui = new TextoveRozhrani(hra);
				//spuštění hry - zádávání příkazů v CLI
				ui.hraj();
			} else {
				System.out.println("Neplatný parametr");
			}
		}
	}
}
