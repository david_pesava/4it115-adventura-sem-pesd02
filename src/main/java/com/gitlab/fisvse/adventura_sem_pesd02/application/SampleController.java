package com.gitlab.fisvse.adventura_sem_pesd02.application;

import java.util.Observer;

import java.util.Observable;

import com.gitlab.fisvse.adventura_sem_pesd02.logika.*;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Trida SampleControler - kontroler apliace a javaFX 
 * 
 *
 * @author  David Pešava, Filip Vencovský
 * @version pro školní rok 2018/2019
 */

public class SampleController implements Observer {
	
	//deklarace stejných prvků jako jsme deklarovali v FXML 
	//názvy musí sedět s fx:id
	//datové typy musí sedět s názvem elementu FXML
	@FXML Button tlaciko;
	@FXML TextField vstup;
	@FXML TextArea vystup;
	@FXML ListView<Prostor> seznamMistnostiListView;
	@FXML ImageView hrac;
	@FXML ListView<Vec> seznamVeciListView;
	@FXML MenuItem menuNapoveda;
	@FXML MenuItem menuNovaHra;
	@FXML MenuItem menuKonec;
	@FXML ImageView predmet1;
	@FXML ImageView predmet2;
	@FXML ImageView predmet3;
	@FXML ImageView predmet4;
	@FXML ImageView predmet5;
	
	private ImageView[] obrazky;

	

	
	
	//budeme si pamatovat odkaz na běžící hru
	private IHra hra;
	@FXML
	/**
	 * metoda odeslat se zavolá podle naší deklarace onAction v FXML po stisknutí tlačítka
	 */
	public void odeslat() {
		//obsah prvku TextField předáme hře a do proměnné vysledek uložíme textový výpis, který nám hra vrátí
		String vysledek = hra.zpracujPrikaz(vstup.getText());
		
		//formátování výstupu do prvku TextArea, přidáváme nový text pomocí appendText
		vystup.appendText("\n\n");
		
		//vložení našeho příkazu z pole TextField, pro lepší komunikaci toho, co se děje
		vystup.appendText(vstup.getText());
		vystup.appendText("\n\n");
		
		//vložení výstupu, který nám hra předtím vrátila do TextArea
		vystup.appendText(vysledek);
		
		//vyprázdnění TextFieldu, abychom mohli zadávat další příkazy
		vstup.setText("");
		
		//zjištění, zda po zpracování nového příkazu neskončila hra
		if(hra.konecHry()) {
			// pokud skončila, zabráníme uživateli zadávání nových příkazů, komunikujeme změnu "prošedivěním" TextFieldu 
			vstup.setDisable(true);
		}
	}

	
	@FXML
	public void menuKonecKlik(ActionEvent event) 
	{
		//ukončení okna/aplikace
		System.exit(0);
	
	}

	
	/**
	 * metoda menuNovaHraKlik se zavolá po kliknutí v menu na položku nová hra, proběhně reset hernho plánu a 
	 * vyčištění inputů a outputů
	 */
	
	@FXML
	public void menuNovaHraKlik(ActionEvent event) 
	{
		
			//reset herního plánu
            hra = new Hra();                                        	
            vystup.setText(hra.vratUvitani());
            vstup.requestFocus();		
            seznamMistnostiListView.getItems().clear();
          	seznamMistnostiListView.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
          		
          	
          	//přiřazení obrázků prázdného batohu
		    obrazky[1].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
		    obrazky[2].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
		    obrazky[3].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
		    obrazky[4].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));

		    
		    //základní hráčská pozice
           	hrac.setX(0);
			hrac.setY(0);
	}
	
	/**
	 * volá se po kliknutí na položku nápověda v menu
	 */
	@FXML
	public void menuNapovedaKlik(ActionEvent event) 
	{
        
		//inicializace stage (okna)
		Stage stage = new Stage();
        stage.setTitle("Nápověda");

        //přiřazení webview pro zobrazení html souboru
        WebView webView = new WebView();
        webView.getEngine().load(com.gitlab.fisvse.adventura_sem_pesd02.application.Main.class.getResource("help.html").toExternalForm());

        //vytvoření a zobrazení okna
        stage.setScene(new Scene(webView, 800, 500));
        stage.show();
		
	}



	
	
	
		
	/**
	 * metoda slouží k předání instance běžící hry kontroleru
	 * lze použít i pro akce, které se mají stát úplně na začátku, ještě předtím, 
	 * než uživatel interaguje s apliakcí
	 * @param hra
	 */
	public void inicializuj(IHra hra) {
		//uložení lokální proměnné hra mezi vlastnosti celého objektu
				this.hra = hra;
				
				//zaregistrování se jako pozorovatel herního plánu
				hra.getHerniPlan().addObserver(this);
				
				//zaregistrování se jako pozorovatel všech prostorů
				for(Prostor prostor : hra.getHerniPlan().getSeznamProstoru()) {
					prostor.addObserver(this);
				}
				
				//přidání observeru na batoh
				hra.getHerniPlan().getBatoh().addObserver(this);	
				
				
				
				//inicializace obrázku pole k image view z java FX
				obrazky = new ImageView[5];
				obrazky[0] = predmet1;
				obrazky[1] = predmet2;
				obrazky[2] = predmet3;
				obrazky[3] = predmet4;
				obrazky[4] = predmet5;
				
				
				//přiřazení uákladních obrázků
				obrazky[1].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    obrazky[2].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    obrazky[3].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    obrazky[4].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));

			    
						
				//podmínky pro zobrazení obrázků - zda se nachází v batohu, jsou zobrazeny
			    if(hra.getHerniPlan().getBatoh().obsahujeVec("Dýka")) {
							obrazky[0].setImage(new Image("https://larpwarriors.co.uk/media/catalog/product/cache/4/image/800x800/9df78eab33525d08d6e5fb8d27136e95/e/l/elven_dagger_large.jpg"));
					    } else {
							obrazky[0].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
					    }
					    if(hra.getHerniPlan().getBatoh().obsahujeVec("Zásoby_jídla")) {
							obrazky[1].setImage(new Image("http://icons.iconarchive.com/icons/sonya/swarm/256/Fast-Food-icon.png"));
					    }else {
							obrazky[1].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
					    }
					    if(hra.getHerniPlan().getBatoh().obsahujeVec("Elfské_brnění")) {
							obrazky[2].setImage(new Image("https://vignette.wikia.nocookie.net/chroniclesofarn/images/2/2e/Emerald-leather-01.jpg/revision/latest/top-crop/width/220/height/220?cb=20110921152920"));
					    }else {
							obrazky[2].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
					    }			   			
					    if(hra.getHerniPlan().getBatoh().obsahujeVec("Lektvar_proti_trolům")) {
							obrazky[3].setImage(new Image("https://www.merlinspotions.com/media/catalog/product/cache/1/small_image/240x300/beff4985b56e3afdbeabfc89641a4582/l/e/led_potion.jpg"));
					    }else {
							obrazky[3].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
					    }
					    if(hra.getHerniPlan().getBatoh().obsahujeVec("Protijed")) {
						    obrazky[4].setImage(new Image("https://vignette.wikia.nocookie.net/harrypotter/images/e/eb/PDS.png/revision/latest?cb=20140730215243"));
					    }else {
							obrazky[4].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
					    }
			    
			    
				
				//vypsání uvítacího textu do TextArea
				vystup.setText(hra.vratUvitani());
				
				
				
				seznamMistnostiListView.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
				//seznamVeciListView.getItems().addAll(hra.getHerniPlan().getBatoh().getBatoh());

				
				//nastavení souřadnic na mapě 
				hrac.setX(hra.getHerniPlan().getAktualniProstor().getPoziceX());
				hrac.setY(hra.getHerniPlan().getAktualniProstor().getPoziceY());
			
	}
			@Override
			public void update(Observable o, Object arg) {
				//vyčištění seznamu východů
				seznamMistnostiListView.getItems().clear();
				
				//naplnění ListView seznamem aktuálních východů
				seznamMistnostiListView.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

			
				//nastavení souřadnic na mapě  
				hrac.setX(hra.getHerniPlan().getAktualniProstor().getPoziceX());
				hrac.setY(hra.getHerniPlan().getAktualniProstor().getPoziceY());
				
				
				//podmínky pro zobrazení obrázků - zda se nachází v batohu, jsou zobrazeny
			    if(hra.getHerniPlan().getBatoh().obsahujeVec("Dýka")) {
					obrazky[0].setImage(new Image("https://larpwarriors.co.uk/media/catalog/product/cache/4/image/800x800/9df78eab33525d08d6e5fb8d27136e95/e/l/elven_dagger_large.jpg"));
			    } else {
					obrazky[0].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    }
			    if(hra.getHerniPlan().getBatoh().obsahujeVec("Zásoby_jídla")) {
					obrazky[1].setImage(new Image("http://icons.iconarchive.com/icons/sonya/swarm/256/Fast-Food-icon.png"));
			    }else {
					obrazky[1].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    }
			    if(hra.getHerniPlan().getBatoh().obsahujeVec("Elfské_brnění")) {
					obrazky[2].setImage(new Image("https://vignette.wikia.nocookie.net/chroniclesofarn/images/2/2e/Emerald-leather-01.jpg/revision/latest/top-crop/width/220/height/220?cb=20110921152920"));
			    }else {
					obrazky[2].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    }			   			
			    if(hra.getHerniPlan().getBatoh().obsahujeVec("Lektvar_proti_trolům")) {
					obrazky[3].setImage(new Image("https://www.merlinspotions.com/media/catalog/product/cache/1/small_image/240x300/beff4985b56e3afdbeabfc89641a4582/l/e/led_potion.jpg"));
			    }else {
					obrazky[3].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    }
			    if(hra.getHerniPlan().getBatoh().obsahujeVec("Protijed")) {
				    obrazky[4].setImage(new Image("https://vignette.wikia.nocookie.net/harrypotter/images/e/eb/PDS.png/revision/latest?cb=20140730215243"));
			    }else {
					obrazky[4].setImage(new Image("https://cdn1.iconfinder.com/data/icons/pointed-edge-web-navigation/101/cross-grey-512.png"));
			    }
			    
			}

		
			
			
			
		
	}
	

