/*
 * Třída Pouzij, obsahující příkaz pro používání předmětů
 */
package com.gitlab.fisvse.adventura_sem_pesd02.logika;

import java.util.Observable;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 *
 * @author David Pešava
 * @version pro školní rok 2016/2017
 */
public class PrikazPouzij extends Observable implements IPrikaz 
{
	
	
	@FXML ListView<Prostor> seznamMistnostiListView;

	
    private static final String NAZEV = "použij";
    private HerniPlan plan;
    private Hra hra;
    //konstruktor
    public PrikazPouzij(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    /**
     * Metoda pro rozhodování se ve hře
     * @ return vrací při správném provdení příkazu hlášku o úspěchu a seznam východů
     * v opačném případě ukončí hru
     */
    @Override
    public String provedPrikaz(String... parametry) {
     
    Prostor aktualniProstor = plan.getAktualniProstor();
    
        if(aktualniProstor.getPouzitelnyPredmet()){
        
         if (parametry.length == 0) {
          return "Zadej,co chceš použít!";
        }
         
        String volba = parametry[0];
        
        //podmínka, která ověřuje,zda je volba od hráče stejná,jako nastavená
        //volba prostoru
        String uzitnaVec = aktualniProstor.getPouzitelnaVec().getNazev();
        if(volba.equals(uzitnaVec)){
            aktualniProstor.setVychod(aktualniProstor.getVychod());
            System.out.println(aktualniProstor.getSpravnePou());
            aktualniProstor.odeberVec(uzitnaVec);
            setChanged();
            notifyObservers();
            return aktualniProstor.popisVychodu() +"  "+aktualniProstor.getSpravnePou(); 
        }
        else{ 
         
            return "Zkus použít něco jiného";  
        }
        
        }
        
        else return "Toto nelze použít";
    
    }
    
    
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
