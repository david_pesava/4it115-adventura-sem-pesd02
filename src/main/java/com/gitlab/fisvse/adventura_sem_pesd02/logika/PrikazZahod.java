/*
 * Trida  prikazZahod pro vykonavani prikazu zahod
 */
package com.gitlab.fisvse.adventura_sem_pesd02.logika;

/**
 *
 * @author David Pešava
 * @version  pro školní rok 2016/2017
 */
public class PrikazZahod implements IPrikaz
{
    private static final String NAZEV = "zahod";
    private HerniPlan plan;
    private Batoh batoh;

    /**
     * Konstruktor
     *
     * @param plan
     */
    public PrikazZahod(HerniPlan plan)
    {
        this.plan = plan;
        this.batoh = plan.getBatoh();
    }

    /**
     * Provadi prikaz "odhod". Odhodi zadanou vec z batohu do daneho prostoru.
     *
     * @param parametry     vec, kterou chceme odhodit
     * @return   zprava, kterou vypise hra hraci
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybi druhe slovo (nazev odhazovane veci), tak ....
            return "Pokud chcete něco odhodit, musíte napsat co....";
        }

        String odhazovanaVec = parametry[0];

        
        Prostor aktualniProstor = plan.getAktualniProstor();
        Vec vec = batoh.getVec(odhazovanaVec);

        if (vec == null) {
            return odhazovanaVec + " neni v batohu.";
        }
        else {
            batoh.odeberVec(odhazovanaVec);
            batoh.zahodZbatohu(vec,odhazovanaVec);
            aktualniProstor.vlozVec(vec);
            return  odhazovanaVec + " bylo zahozeno, nyní je v tomto prostoru";
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
