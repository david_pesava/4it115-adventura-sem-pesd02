/*
 * 
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.fisvse.adventura_sem_pesd02.logika;
import java.util.Random;

/**
 *
 * @author David Pešava
 * @version pro školní rok 2016/2017
 */
public class PrikazRemcej implements  IPrikaz{
    
    private static final String NAZEV = "remcej";
    private HerniPlan plan;
    
    public PrikazRemcej(HerniPlan plan)
    {
        this.plan = plan;
    }
     @Override
    public String provedPrikaz(String... parametry)
    {
     Random randomGenerator = new Random();
     int nahodne = randomGenerator.nextInt(5);
     switch(nahodne){
         case 0: return "Thórin: Povězte, pane Pytlíku, bojoval jste často?\n" +
                        "Bilbo: Promiňte?\n" +
                        "Thórin: Sekera nebo meč? Jakou zbraň volíte?\n" +
                        "Bilbo: Jsem dobrý v házení kaštanů, když to chcete vědět, ale nechápu, proč vás to zajímá.";
         case 1: return "Bilbo: Gandalfe, kam jdete?\n" +
                        "Gandalf: Uchýlím se do společnosti toho jediného, kdo tady má rozum.\n" +
                        "Bilbo: A to je kdo?\n" +
                        "Gandalf: Jsem to já, pane Pytlíku!";
         case 2: return "Bilbo Pytlík: Dobré ráno.\n" +
                        "Gandalf: Co tím myslíte? Přejete mi dobré ráno? Nebo tím myslíte, že je dobré, ať o to stojím, nebo ne? Či že se právě dnes ráno cítíte dobře? Anebo že je jitro jako stvořené, aby byl člověk dobrý?\n" +
                        "Bilbo Pytlík: Asi všechno najednou.";
         case 3: return "Bilbo: \"Gandalf? Vy ještě provozujete své řemeslo?\"\n" +
                        "Gandalf (podezřívavě): \"A co jiného bych měl asi tak provozovat?!\"";
         case 4: return "Bilbo: \"Ne, počkat! Výprava se musí vrátit!\"\n" +
                         "Thorin: \"Proč?\"\n" +
                         "Bilbo: \"Omlouvám se, ale zapomněl jsem kapesník.\"";
         default:return "Radši nic";
     }
    }
    
    
    @Override
    public String getNazev()
    {
        return NAZEV;
    }
}
