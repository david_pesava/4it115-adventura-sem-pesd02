/*
 * Třída pro příkaz batoh, která spravuje hráčův inventář
 */

package com.gitlab.fisvse.adventura_sem_pesd02.logika;



/**
 *
 * @author David Pešava
 * @version  pro školní rok 2016/2017
 */
public class PrikazBatoh implements IPrikaz      
{
     private static final String NAZEV = "batoh";
     private HerniPlan plan;
     /*
     * Konstruktor.
     */
    public PrikazBatoh(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     * Provadi prikaz "batoh". Vypisuje vsechny veci, ktere jsou v batohu.
     * @param parametry     parametr muze byt cokoliv
     * @return   zprava, kterou vypise hra hraci
     */
    
    @Override
    public String provedPrikaz(String... parametry)
    {
        if(plan.getBatoh().obsahBatohu().equals(""))
        {
            return "Batoh je prazdny.";
        }
        return "V batohu je: " + plan.getBatoh().obsahBatohu();
    }

    /**
     * Metoda vraci nazev prikazu.
     */
    @Override
    public String getNazev()
    {
        return NAZEV;
    }
    
    
    
   
    
}
