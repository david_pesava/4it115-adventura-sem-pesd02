/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code HraTest} slouží ke komplexnímu otestování
 * třídy {@link HraTest}.
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class HraTest
{
    private Hra hra1;
    //== KONSTANTNÍ ATRIBUTY TŘÍDY =============================================
    //== PROMĚNNÉ ATRIBUTY TŘÍDY ===============================================
    //== STATICKÝ INICIALIZAČNÍ BLOK - STATICKÝ KONSTRUKTOR ====================
    //== KONSTANTNÍ ATRIBUTY INSTANCÍ ==========================================
    //== PROMĚNNÉ ATRIBUTY INSTANCÍ ============================================
    //== PŘÍSTUPOVÉ METODY VLASTNOSTÍ TŘÍDY ====================================
    //== OSTATNÍ NESOUKROMÉ METODY TŘÍDY =======================================

    //##########################################################################
    //== KONSTRUKTORY A TOVÁRNÍ METODY =========================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------
    //== PŘÍPRAVA A ÚKLID PŘÍPRAVKU ============================================

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
        hra1 = new Hra();
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    //== PŘÍSTUPOVÉ METODY VLASTNOSTÍ INSTANCÍ =================================
    //== OSTATNÍ NESOUKROMÉ METODY INSTANCÍ ====================================
    //== SOUKROMÉ A POMOCNÉ METODY TŘÍDY =======================================
    //== SOUKROMÉ A POMOCNÉ METODY INSTANCÍ ====================================
    //== INTERNÍ DATOVÉ TYPY ===================================================
    //== VLASTNÍ TESTY =========================================================
    //
    //     /********************************************************************
    //      *
    //      */
    //     @Test
    //     public void testXxx()
    //     {
    //     }

    @Test
    public void testProhra1()
    {
        
        hra1.zpracujPrikaz("jdi Roklinka");
        hra1.zpracujPrikaz("jdi Trolí_tábor");
        assertEquals(false, hra1.getHerniPlan().getBatoh().obsahujeVec("Lektvar_proti_trolů"));
    }
    
     @Test
    public void testProhra2()
    {
        hra1.zpracujPrikaz("jdi Roklinka");
        hra1.zpracujPrikaz("jdi Trolí_tábor");
        assertEquals(false, hra1.getHerniPlan().getBatoh().obsahujeVec("Lektvar_proti_trolů"));
    }
    
    @Test
    public void testProhra3()
    {
        hra1.zpracujPrikaz("jdi Roklinka");
        hra1.zpracujPrikaz("jdi Mlžné_hory");
        hra1.zpracujPrikaz("jdi Skřetí_jeskyně");
        assertEquals(false, hra1.getHerniPlan().getBatoh().obsahujeVec("Elské_brnění"));
    }
    
    @Test
    public void testProhra4()
    {
        hra1.zpracujPrikaz("jdi Roklinka");
        hra1.zpracujPrikaz("seber Elské_brnění");
        hra1.zpracujPrikaz("jdi Mlžné_hory");
        hra1.zpracujPrikaz("jdi Skřetí_jeskyně");
        assertEquals(false, hra1.getHerniPlan().getBatoh().obsahujeVec("Elské_brnění"));
        hra1.zpracujPrikaz("jdi Glumova_jeskyně");
        assertEquals("Provedl jsi špatnou volbu a proto zde hra pro tebe končí.",hra1.zpracujPrikaz("volba test") );
    }
    @Test
     public void testProhra5()
    {
        hra1.zpracujPrikaz("jdi Roklinka");
        hra1.zpracujPrikaz("seber Elské_brnění");
        hra1.zpracujPrikaz("jdi Mlžné_hory");
        hra1.zpracujPrikaz("jdi Skřetí_jeskyně");
        assertEquals(false, hra1.getHerniPlan().getBatoh().obsahujeVec("Elské_brnění"));
        hra1.zpracujPrikaz("jdi Planina");
        hra1.zpracujPrikaz("seber Protijed");
        hra1.zpracujPrikaz("jdi Temný_hvozd");
        assertEquals(false, hra1.getHerniPlan().getBatoh().obsahujeVec("Protijed"));
        
    }

    @Test
    public void testVyhra()
    {
        hra1.zpracujPrikaz("jdi Roklinka");
        hra1.zpracujPrikaz("seber Elfské_brnění");
        hra1.zpracujPrikaz("jdi Zahrady");
        hra1.zpracujPrikaz("seber Lektvar_proti_trolů");
        hra1.zpracujPrikaz("jdi Mlžné_hory");
        hra1.zpracujPrikaz("použij Klacík");
        hra1.zpracujPrikaz("jdi Skřetí_jeskyně");
        hra1.zpracujPrikaz("jdi Planina");
        hra1.zpracujPrikaz("seber Protijed");
        hra1.zpracujPrikaz("jdi Temný_hvozd");
        hra1.zpracujPrikaz("jdi Jezerní_město");
        hra1.zpracujPrikaz("použij Balista");
        hra1.zpracujPrikaz("seber Dýka");
        hra1.zpracujPrikaz("jdi Osamělá_hora");
        hra1.zpracujPrikaz("volba Bezpečný_zadní_vchod");
        hra1.zpracujPrikaz("jdi Dračí_místonst");
        hra1.zpracujPrikaz("jdi Bonusový_úkol");
        hra1.zpracujPrikaz("jdi Vítězné_místo");
        assertEquals(true, hra1.konecHry());
    }
}


