/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package com.gitlab.fisvse.adventura_sem_pesd02.logika;



/*******************************************************************************
 * Instance třídy Vec představují ...
 *
 * @author    David Pešava
 * @version   pro školní rok 2016/2017
 */
public class Vec
{
    //== Datové atributy (statické i instancí)======================================
    private String nazev;
    private boolean prenositel;
    private boolean pouzitelna;
    private HerniPlan plan;
    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public Vec(String nazev, boolean prenositel, boolean pouzitelna)
    {
        this.nazev = nazev;
        this.prenositel = prenositel;
        this.pouzitelna = pouzitelna;
    }
    
    
    
    
    
    
    //== Nesoukromé metody (instancí i třídy) ======================================
    public String getNazev(){
        return nazev;
    }
    public boolean isPrenositel(){
        return prenositel;
    }
    public boolean isPouzitelna(){
        return pouzitelna;
    }
  

    //== Soukromé metody (instancí i třídy) ========================================

}
