/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package com.gitlab.fisvse.adventura_sem_pesd02.logika;





/* 
 *
 * @author    David Pešava
 * @version   pro školní rok 2016/2017
 */
public class PrikazSeber implements IPrikaz
{
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazSeber(HerniPlan plan) {
        this.plan = plan;
        this.batoh = plan.getBatoh();
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Co mám sebrat? Musíš zadat jaký předmět";
        }

        String nazevVeci = parametry[0];

        // zkoušíme přejít do sousedního prostoru

        Prostor aktualniProstor = plan.getAktualniProstor();
        Vec sbiranaVec = aktualniProstor.odeberVec(nazevVeci);
         
        
        if (sbiranaVec== null) {
            return "To tu není!";
        }
        else {
            if (sbiranaVec.isPrenositel()){ 
                // třeba se zeptat zda se věc vejde do batohu, pokud ano, 
                //vloží se, pokud ne tak se musí vrátit
                batoh.pridejVec(sbiranaVec);
                return "Věc " + sbiranaVec.getNazev() + " zmizi z prostoru a"
                        + "a nyní se nachází ve vašem batohu.";
                
                
            }
            else{
                aktualniProstor.vlozVec(sbiranaVec);
                return "to neuzvedneš!";
            }
            
        }
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
    
    
}
