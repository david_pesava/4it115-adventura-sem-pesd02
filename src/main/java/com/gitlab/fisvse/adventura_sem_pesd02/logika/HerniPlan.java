package com.gitlab.fisvse.adventura_sem_pesd02.logika;

import java.util.HashSet;

import java.util.Observable;
import java.util.Set;



/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, David Pešava
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan extends Observable {
    
    private Prostor aktualniProstor;
    
    private Prostor vyherniProstor;
    
    private Batoh batoh;
    
    private Set<Prostor> seznamProstoru;
  
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
    	seznamProstoru = new HashSet<>();
        zalozProstoryHry();
        batoh = new Batoh();
        batoh.pridejVec(new Vec("Dýka",true,false));
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    public void zalozProstoryHry() {
        
        
        
         //předmeěty hry - vytvoření
         //předměty nůtné
        Vec zasobyJidla = new Vec("Zásoby_jídla",true,false);
        Vec lektarProtiTrolum = new Vec("Lektvar_proti_trolům",true,false);
        Vec elskeBrneni = new Vec("Elfské_brnění",true,false);
        Vec protiJed = new Vec("Protijed",true,false);
      //  Vec dyka = new Vec("Dýka",true,false);
        //předměty nutné, nepřenositelné
        Vec klacik = new Vec("Klacík",false,true);
        Vec balista = new Vec("Balista",false,true);
        
        //nepodstatné veci
        /*
        Vec voda = new Vec("Voda",true,false);
        Vec dzban = new Vec("Džbán",true,false);
        Vec loutna = new Vec("Loutna",true,false);
        Vec svicka = new Vec("Svíčka",true,false);
        Vec hodinky = new Vec("Hodinky",true,false);
        Vec cepice = new Vec("Čepice",true,false);
        Vec kamen = new Vec("Kámen",true,false);
        Vec vetvicka = new Vec("Větvička",true,false);
        Vec hracka = new Vec("Hračka",true,false);
        Vec udice = new Vec("Udice",true,false);
        Vec bota = new Vec("Bota",true,false);
        Vec lahev = new Vec("Lahev",true,false);
        Vec pivo = new Vec("Pivo",true,false);
        */
        
       // Vec mince = new Vec("Bedna_zlatých mincí",true,false);
        

        
        //nepodstatné a nepřenostitelné
        Vec stul = new Vec("Stůl",false,false);
        Vec troliPalice = new Vec("Trolí palice",false,false);
        Vec trun = new Vec("Trůn",false,false);
        Vec kmen = new Vec("Kmen",false,false);
        Vec povoz = new Vec("Koňsý povoz",false,false);
        Vec draciLebka = new Vec("Dračí lebka",false,false);
      
        
        // vytvářejí se jednotlivé prostory
        Prostor kraj = new Prostor("Kraj","Roklinka - Hobití vesnice, ze které Bilbo pochází.Odsud vyrazíš na dobrodružství.",5,115);
        Prostor roklinka = new Prostor("Roklinka","Elfské město, ve kterém se dále nachází zahrady a palác.",100,115);
        Prostor roklinkaZahrada = new Prostor("Zahrady","zahrady elfského město",100,180);
        Prostor roklinkaPalac = new Prostor("Palác","palác elfského město",100,60);
        
        Prostor troliTabor = new Prostor("Trolí_tábor","Tábor zlých trolů,pokud se ti podaří přežít, možná zde něco najdeš",true,"Lektvar_proti_trolům"," a proto tě trolé sežrali!",200,60);

        Prostor planina = new Prostor("Planina","Planina - tady nic není, tak co tu?",400,115);
        Prostor glumovaJeskyne = new Prostor("Glumova_jeskyně","Jeskyně, ve které se nachází glum, neche te odsud pustit a musíš si s ním zajrát o svůj život",true,"hora","odvověz na tuto hádnaku: \n Kořeny má škryté v žemi, \n vypíná še nad jedlemi,\n štoupá pořád výš a výš,\n ale růšt ji nevidíš. \n pro odpověď použij příkaz volba",planina,330,60);
        Prostor skretiJeskyne = new Prostor("Skřetí_jeskyně","Jeskyně plná skřetů, aby toho nebylo málo, všimli si vás, je čas zdrhnout",true,"Elfské_brnění"," a proto ti sekere skřeta vjela přímo do hrudi",300,115);

        Prostor mlzneHory = new Prostor("Mlžné_hory","Mlžné hory, a zde jsi narazil na skřety, stojíte na úzké horské cestě, u cesty vidíš obrovskou hromadu kamení, ze které čouhá klacík. ",true,klacik,"udělej něco pro záchranu celé party!","Vytáhl jsi klacík z hromady, ten uvolnil kameny a ty zasypaly všechny skřety",skretiJeskyne,200,115);
        Prostor temnyHvozd = new Prostor("Temný_hvozd","Temný hvozd - na vaši skupinu zautočili gigantičtí pavouci a jeden z nich bodl Bilba svým jedovým ostnem, pokud máš protijed, můžeš být v klidu a utíkat, než bodnou někoho dalšího",true,"Protijed"," a proto se Bilbo otrávil pavoučím jedem",500,115);
       
        
        Prostor osmelaHoraDraciMistnost = new Prostor("Dračí_místonst","Dračí mistnost,Zde se nachází drak, zkoušíš se k němu proplížit a bodnout ho do srdce",true,"Dýka","a proto,že že nemáš, jak draka zabít, esžral tě",700,60);
        Prostor osamelaHora = new Prostor("Osamělá_hora","Osamělá hora, do které vedouo tři vchody",true,"Bezpečný_zadní_vchod","tady jsou možnosti: \n Bezpečný_zadní_vchod \n Nebezpečný_přední_vchod \n   ",osmelaHoraDraciMistnost,660,115);
        Prostor jezerniMesto = new Prostor("Jezerní_město","Jezerní město - Město postavené na jezeře, zrovna na něj útočí drak!",true,balista,"Něco najdi a použij!","Výstřelem z balisty jsi zasáhl draka, ten se zraněný raději stáhl, obyvatelé Jezerního městě tě mají za hrdninu. ",osamelaHora,580,115);

        
        Prostor osmelaHoraBonus = new Prostor("Bonusový_úkol","Podařilo se ti zabít draka a teď jis v pokladnici! je zde plno vzácncýh předmětů.naber si co můžeš a pokračuj dále",700,60);
        Prostor vitezneMisto = new Prostor("Vítězné_místo","Vyhrál jsi!",660,115);
        

        

        
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        kraj.setVychod(roklinka);
        roklinka.setVychod(kraj);
        roklinka.setVychod(roklinkaPalac);
        roklinka.setVychod(roklinkaZahrada);
        roklinka.setVychod(mlzneHory);
        roklinka.setVychod(troliTabor);
        troliTabor.setVychod(roklinka);
        roklinkaPalac.setVychod(roklinka);
        roklinkaPalac.setVychod(roklinkaZahrada);
        roklinkaZahrada.setVychod(roklinkaPalac);
        roklinkaZahrada.setVychod(roklinka);
        skretiJeskyne.setVychod(glumovaJeskyne);        
        skretiJeskyne.setVychod(planina); 
        planina.setVychod(temnyHvozd);
        temnyHvozd.setVychod(jezerniMesto);
        osmelaHoraDraciMistnost.setVychod(osmelaHoraBonus);
        osmelaHoraBonus.setVychod(vitezneMisto);
        
        mlzneHory.setVychod(skretiJeskyne);
        glumovaJeskyne.setVychod(planina);
        jezerniMesto.setVychod(osamelaHora);
        osamelaHora.setVychod(osmelaHoraDraciMistnost);



        //vložení předmětů
        kraj.vlozVec(zasobyJidla);
        roklinkaZahrada.vlozVec(lektarProtiTrolum);
        roklinka.vlozVec(elskeBrneni);
        planina.vlozVec(protiJed);
        mlzneHory.vlozVec(klacik);
       // jezerniMesto.vlozVec(dyka);
        jezerniMesto.vlozVec(balista);
        
        //vložení nepodstatných předmětů
        /*
        kraj.vlozVec(voda);
        roklinka.vlozVec(dzban);
        roklinka.vlozVec(loutna);
        roklinkaZahrada.vlozVec(svicka);
        troliTabor.vlozVec(pivo);
        glumovaJeskyne.vlozVec(bota);
        skretiJeskyne.vlozVec(hodinky);
        jezerniMesto.vlozVec(cepice);
        jezerniMesto.vlozVec(lahev);
        jezerniMesto.vlozVec(udice);
        osamelaHora.vlozVec(hracka);
        mlzneHory.vlozVec(kamen);
        temnyHvozd.vlozVec(kamen);
        mlzneHory.vlozVec(vetvicka);
        temnyHvozd.vlozVec(vetvicka);
       
        osmelaHoraBonus.vlozVec(mince2);
        osmelaHoraBonus.vlozVec(mince3);
        osmelaHoraBonus.vlozVec(mince4);
        osmelaHoraBonus.vlozVec(mince5);
        osmelaHoraBonus.vlozVec(mince6);
        osmelaHoraBonus.vlozVec(mince7);
        */
       // osmelaHoraBonus.vlozVec(mince);
        
        //vložení nepodstatných nepřenosných věcí
        roklinka.vlozVec(stul);
        troliTabor.vlozVec(troliPalice);
        roklinkaPalac.vlozVec(trun);
        planina.vlozVec(kmen);
        temnyHvozd.vlozVec(povoz);
        osmelaHoraBonus.vlozVec(draciLebka);
        
       
    
        
        
                
       aktualniProstor = kraj; // hra začíná v Kraji
    
       vyherniProstor = vitezneMisto;
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    public Batoh getBatoh() { 
        return batoh;
    }
    
    
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
       setChanged();
       notifyObservers();

    }
    
    public boolean jeVyhra(){
        return aktualniProstor == vyherniProstor;
    }
    
    public Set<Prostor> getSeznamProstoru() {
		return seznamProstoru;
				
	}

    

}
