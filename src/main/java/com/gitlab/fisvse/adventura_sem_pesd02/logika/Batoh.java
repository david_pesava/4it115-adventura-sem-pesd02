package com.gitlab.fisvse.adventura_sem_pesd02.logika;

import  java.util.*;

/**
 * Trida batoh obsahuje kod pro vkládání věcí do batohu.
 * V batohu mohou byt maximalne N navolených věcí dle proměné KAPACiTA
 *
 * @author  David Pešava
 * @version pro školní rok 2016/2017
 */
public class Batoh extends Observable
{
    private static final int KAPACITA = 5;
    private Map<String, Vec> seznamVeci;

    
    //Konstruktor 
    public Batoh()
    {
        seznamVeci = new HashMap<>();
    }

    /**
     * Metoda zjistuje, zda je jeste misto v batohu.
     *
     * @return vraci true, pokud je jeste v batohu misto;
     *      
     */
    public boolean mistoVBatohu()
    {
        return (seznamVeci.size() < KAPACITA);
    }

    /**
     * Metoda pro pridani veci do batohu
     *
     * @ param vec      pridavana vec
     * @ return     vraci hodnotu true,pokud byla vec pridana do batohu;
     *                    v opacnem pripade vraci hodnotu false
     */
    public boolean pridejVec (Vec vec)
    {
        if(mistoVBatohu()) {
            seznamVeci.put(vec.getNazev(), vec);
            setChanged();
    	    notifyObservers();

            return true;
        }
        return false;
    }

    
    public boolean zahodZbatohu (Vec vec,String nazevVeci) { 
        seznamVeci.remove(vec,nazevVeci);
    	setChanged();
	    notifyObservers();
	    return true;
    }
    
    
    /**
     * Metoda pro odebirani veci z batohu.
     *
     * @param nazev  nazev odebirane veci
     * @ return        vraci hodnotu true pokud byla vec v batohu a byla odebrana;
     *                      v opacnem pripade vraci false
     */
    public boolean odeberVec(String nazev) {
        if(seznamVeci.containsKey(nazev) && seznamVeci.get(nazev).isPrenositel()) {
            seznamVeci.remove(nazev);
            setChanged();
    	    notifyObservers();
    	    return true;

        }
        return false;
    }

    /**
     * Metoda pro zjisteni, zda je dana vec v batohu.
     *
     * @param nazev      nazev veci na kterou se ptame
     * @return      vraci hodnotu true, pokud se dana vec v batohu vyskytuje;
     *                     v opacnem pripade vraci false
     */
    public boolean obsahujeVec(String nazev)
    {
        return (seznamVeci.containsKey(nazev));
    }

    /**
     * Metoda pro ziskani zadane veci.
     *
     * @param nazev     nazev hledane veci
     * @return vec
     */
    public Vec getVec(String nazev) {
        return seznamVeci.get(nazev);
    }

    /**
     * Metoda pro zjisteni obsahu batohu.
     *
     * @return      vraci seznam veci v batohu
     */
    public String obsahBatohu() {
        StringBuffer vystup = new StringBuffer();
        boolean prvniZnak = true;
        for (String vec : seznamVeci.keySet()) {
            if(!prvniZnak) {
                vystup.append(',');
                vystup.append(' ');  
            }
            prvniZnak = false;
            vystup.append(vec);
        }
        return vystup.toString();
    }
    
  
   
    
    public Collection<Vec> getBatoh() {
		return seznamVeci.values();
	}
    

 
    
}
