/*
 * Třída pro příkaz prozkoumej, která odhaluje předměty v prostoru
 */
package com.gitlab.fisvse.adventura_sem_pesd02.logika;


/**
 *
 * @author David Pešava
 * @version pro školní rok 2016/2017
 */
public class PrikazProzkoumej implements IPrikaz{
     private static final String NAZEV = "prozkoumej";
     private HerniPlan plan;
   
   
    public PrikazProzkoumej(HerniPlan plan) {
        this.plan = plan;
     
    }

    @Override
    public String provedPrikaz(String... parametry) {
     
    Prostor aktualniProstor = plan.getAktualniProstor();
  
    return aktualniProstor.popisVeci();
    }
    
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
