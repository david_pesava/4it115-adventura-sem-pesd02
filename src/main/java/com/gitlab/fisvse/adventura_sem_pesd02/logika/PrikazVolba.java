package com.gitlab.fisvse.adventura_sem_pesd02.logika;

/**
 *Trida prikaz Volba pro provádění příkazu volba
 * 
 * @author David Pešava
 * @version  pro školní rok 2016/2017
 */
public class PrikazVolba implements IPrikaz{
    
    private static final String NAZEV = "volba";
    private HerniPlan plan;
    private Hra hra;
    //konstruktor
    public PrikazVolba(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    
    /**
     * Metoda pro rozhodování se ve hře
     *
     * 
     * @ return vrací při správném provdení příkazu hlášku o úspěchu a seznam východů
     *                      v opačném případě ukončí hru
     */
    @Override
    public String provedPrikaz(String... parametry) {
     
    Prostor aktualniProstor = plan.getAktualniProstor();
    
        if(aktualniProstor.getVolba()){
        
         if (parametry.length == 0) {
          return "Zadej co bude následovat, bez toho se v příběhu nehneš dále!";
        }
         
        String volba = parametry[0];
        
        //podmínka, která ověřuje,zda je volba od hráče stejná,jako nastavená
        //volba prostoru
        if(volba.equals(aktualniProstor.getSpravnaVolba())){
            aktualniProstor.setVychod(aktualniProstor.getVychod());
            System.out.println("Gratuluji provedl jsi správnou volbu,"
                    + " níže můžeš vidět možné východy:");
            return aktualniProstor.popisVychodu();
        }
        else{ //ukončení hry po špatné volbě
           hra.setKonecHry(true);
            return "Provedl jsi špatnou volbu a proto zde hra pro tebe končí.";  
        }
        
        }
        
        else return "Zde žádná volba není";
    
    }
    
    
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
