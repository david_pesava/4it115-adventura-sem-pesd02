package com.gitlab.fisvse.adventura_sem_pesd02.logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček, David Pešava
 *@version    pro školní rok 2016/2017
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Prostor prostor;
    private Batoh batoh;
    private Hra hra;
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazJdi(HerniPlan plan,Hra hra) {
        this.plan = plan;
        this.batoh = plan.getBatoh();
        this.hra = hra;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);
        
        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        }
        else {
            
            plan.setAktualniProstor(sousedniProstor);
            return sousedniProstor.dlouhyPopis() + nutnyPredmet(sousedniProstor.getNutnypredmet(),
                    sousedniProstor.getNutnypredmetNazev(),sousedniProstor);
        }
    }
    
    /**
     *  Kontroluje,zda hráč má v batohu nutný předmět pro průchod hrou
     *@param parametry - zda má prostor nutný předmět a název předmětu(pokud ho má)                     
     *@return při TRUE nic, při FALSE ukončí hru
     */ 
    private String nutnyPredmet(boolean obsahujePredmet, String nutnyPredmetNazev,Prostor aktProstor){
    if(obsahujePredmet){
    
    boolean obsahujeTo =batoh.obsahujeVec(nutnyPredmetNazev);
        
     if(obsahujeTo){
     return"";
        
    }else{
     hra.setKonecHry(true);
     return "Nemáš u sebe předmět "+ nutnyPredmetNazev + aktProstor.getTextPohromy();
    }
        
    }else return "";
   
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
